//
//  OpenCVUtil.m
//  OpenCVSample
//
//  Created by gibachan on 2014/10/20.
//  Copyright (c) 2014年 gibachan. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>

#import "OpenCVSample-Bridging-Header.h"

@interface Detector()
{
    cv::CascadeClassifier cascade;
    NSInteger face_count;
}
@end

@implementation Detector: NSObject

- (id)init {
    self = [super init];
    
    // 分類器の読み込み
    NSBundle *bundle = [NSBundle mainBundle];
//    NSString *path = [bundle pathForResource:@"haarcascade_frontalface_alt" ofType:@"xml"];
//    NSString *path = [bundle pathForResource:@"1256617233-1-haarcascade_hand" ofType:@"xml"];
//    NSString *path = [bundle pathForResource:@"1256617233-2-haarcascade-hand" ofType:@"xml"];
    NSString *path = [bundle pathForResource:@"palm" ofType:@"xml"];
    std::string cascadeName = (char *)[path UTF8String];
    
    if(!cascade.load(cascadeName)) {
        return nil;
    }
    
    return self;
}

- (UIImage *)recognizeFace:(UIImage *)image {
    
    // UIImage -> cv::Mat変換
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat mat(rows, cols, CV_8UC4);
    
    CGContextRef contextRef = CGBitmapContextCreate(mat.data,
                                                    cols,
                                                    rows,
                                                    8,
                                                    mat.step[0],
                                                    colorSpace,
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault);
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
   
    // 顔検出
    std::vector<cv::Rect> faces;
    cascade.detectMultiScale(mat, faces,
//                             1.1, 2,
//                             1.1, 10,
//                             1.12, 2,
                             1.10, 10, // palm
                             CV_HAAR_SCALE_IMAGE,
                             cv::Size(100, 100)); // palm

    // 顔の位置に丸を描く
    std::vector<cv::Rect>::const_iterator r = faces.begin();
    face_count = 0;
    for(; r != faces.end(); ++r) {
        cv::Point center;
        int radius;
        center.x = cv::saturate_cast<int>((r->x + r->width*0.5));
        center.y = cv::saturate_cast<int>((r->y + r->height*0.5));
        radius = cv::saturate_cast<int>((r->width + r->height) / 3);
        cv::circle(mat, center, radius, cv::Scalar(80,80,255), 3, 8, 0 );
        face_count++;
    }

    // cv::Mat -> UIImage変換
    UIImage *resultImage = MatToUIImage(mat);
    
    return resultImage;
}

- (NSInteger)getFaceCount {
    return face_count;
}

@end
