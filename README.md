# RaisingHandCounter

## install

- Install CocoaPods
    - sudo gem update --system
    - sudo gem install -n /usr/local/bin cocoapods
    - pod setup
- Use CocoaPods
    - cd ~/raising_hand_counter_ios
    - pod install
- Build on Xcode
    - open RaisingHandCounter.xcworkspace